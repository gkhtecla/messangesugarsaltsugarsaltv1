package com.beenaxis.lib.utils

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class PasswordHashTest{

    @Test
    fun equalied(){
        println(PasswordHash.hashSHA("test1"))
        println(PasswordHash.hashSHA("teawdwadst1"))
        println(PasswordHash.hashSHA("testawd1"))
        println(PasswordHash.hashSHA("testdawd1"))
        assertEquals(PasswordHash.hashSHA("test1"), PasswordHash.hashSHA("test1"))
        assertEquals(PasswordHash.hashSHA("testL{FA[31"), PasswordHash.hashSHA("testL{FA[31"))
        assertEquals(PasswordHash.hashSHA("tes4593)_=1=112424"), PasswordHash.hashSHA("tes4593)_=1=112424"))
    }
}