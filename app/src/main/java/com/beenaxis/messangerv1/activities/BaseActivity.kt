package com.beenaxis.messangerv1.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.beenaxis.lib.callbacks.Callback
import com.beenaxis.lib.callbacks.types.NoCallback
import com.beenaxis.messangerv1.server.Connector

/*
    Абстрактность для всех Activity
 */
open class BaseActivity : AppCompatActivity() {

    var needCallbacks:Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        if(Connector.instanced && needCallbacks){
            Connector.instance.currentActivity = this
        }
        super.onCreate(savedInstanceState)
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)
        println("${javaClass.simpleName} : POSTCREATE")
    }

    override fun onStart() {
        super.onStart()
        println("${javaClass.simpleName} : START")
    }

    override fun onStop() {
        super.onStop()
        println("${javaClass.simpleName} : STOP")
    }

    override fun onDestroy() {
        super.onDestroy()
        if(needCallbacks){
            Connector.instance.currentActivity = null
        }
        println("${javaClass.simpleName} : DESTROY")
    }

    open fun onCallback(incoming: Callback): Callback? {
        return NoCallback()
    }
}