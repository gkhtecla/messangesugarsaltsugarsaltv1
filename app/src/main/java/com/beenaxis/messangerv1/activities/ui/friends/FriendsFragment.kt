package com.beenaxis.messangerv1.activities.ui.friends

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.beenaxis.messangerv1.R
import com.beenaxis.messangerv1.activities.ui.BaseFragment

class FriendsFragment : BaseFragment() {

    lateinit var containerr:LinearLayout

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_friends, container, false)
        containerr = root.findViewById(R.id.gallery_cont)
        return root
    }
}