    package com.beenaxis.messangerv1.activities

import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.*
import androidx.annotation.RequiresApi
import com.beenaxis.lib.callbacks.Callback
import com.beenaxis.lib.callbacks.sendCallback
import com.beenaxis.lib.callbacks.types.ErrorCallback
import com.beenaxis.lib.callbacks.types.NoCallback
import com.beenaxis.lib.callbacks.types.SuccessCallback
import com.beenaxis.lib.callbacks.types.groups.GetGroupCallback
import com.beenaxis.lib.callbacks.types.groups.MessageInfo
import com.beenaxis.lib.callbacks.types.groups.SendMessageCallback
import com.beenaxis.messangerv1.R
import com.beenaxis.messangerv1.server.Connector
import com.beenaxis.messangerv1.utils.ID
import com.beenaxis.messangerv1.utils.getGroupInfo
import io.netty.util.concurrent.CompleteFuture
import java.util.concurrent.CompletableFuture

    /**
 * Чат
 */
class ChatActivity : BaseActivity() {

    init{
        needCallbacks = true
    }

    @RequiresApi(Build.VERSION_CODES.N)
    override fun onCallback(incoming: Callback): Callback? {
        if(incoming is SendMessageCallback){
            addMessage(MessageInfo(incoming.fromID, incoming.content, incoming.date))
            return SuccessCallback()
        }
        return NoCallback()
    }

    fun seeInfo(gID:Long){
        val intent = Intent(this, GroupInfoActivity::class.java)
        intent.putExtra("groupID", gID)
        startActivity(intent)
    }

        //Проверка доступа к чату
    fun checkAccess(onAccept:()->Unit, onDecline:()->Unit) {
        var accessed = false
        val id = getGroupID()
        if(id == -1L)return
        getGroupInfo(this, id){
            it.members.forEach {
                if(it.id == Connector.instance.authorizedID){
                    accessed = true
                }
            }
            if(accessed){
                onAccept.invoke()
            }else{
                onDecline.invoke()
            }
        }
    }

    fun getGroupID() : Long{
        return intent.extras?.getLong("groupID") ?: -1L
    }

        //Так как у нас разные потоки по разному откликаются, нужно синхронизировать их
    var loadedMain = false //При загрузке main layoutа
    @RequiresApi(Build.VERSION_CODES.N)
    var future = CompletableFuture<Boolean>() //Фьючерс для синхронизации updateState и onCreate

    @RequiresApi(Build.VERSION_CODES.N)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val groupID = getGroupID()
        if(groupID == -1L){
            finish()
        }
        checkAccess({
            runOnUiThread {
                setContentView(R.layout.activity_chat)
                future.complete(true)
                loadedMain = true
                val back = findViewById<ImageButton>(R.id.chats_back)
                val send = findViewById<ImageButton>(R.id.chats_send)
                val message = findViewById<EditText>(R.id.chats_message)
                back.setOnClickListener {
                    finish()
                }
                send.setOnClickListener {
                    if(message.text.toString().isBlank()){
                        return@setOnClickListener
                    }
                    sendCallback(SendMessageCallback(groupID, Connector.instance.authorizedID, message.text.toString(), System.currentTimeMillis(), Connector.instance.authorizedKey!!)){
                    }.send(Connector.instance.serverChannel!!)
                }
            }
        }, {
            decline()
        })
    }

    fun clearMessages() = runOnUiThread { //Очищаем сообщения из контейнера
        findViewById<LinearLayout>(R.id.chat_msgcont).removeAllViews()
    }

    @RequiresApi(Build.VERSION_CODES.N)
    fun updateChatState(){//Обновить чат
        if(!loadedMain){
            future.get() //Здесь поток будет waiting пока onCreate не возобновит его.
        }
        clearMessages()
        val groupID = getGroupID()
        if(groupID == -1L)return
        updateInfo(groupID){//Получаем чат
            runOnUiThread {//Ивент на клик названия
                val title = findViewById<TextView>(R.id.chats_title)
                title.text = it.name+" | "+it.members.size+" уч."
                title.setOnClickListener { v->
                    seeInfo(groupID)
                }
            }
            var from = it.messages.size-35
            var to = it.messages.size
            if(from < 0){
                from = 0
            }
            if(to > it.messages.size){
                to = it.messages.size
            }
            for(infoID in from until to){
                var info = it.messages[infoID]
                addMessage(info)
            }
        }
    }

    fun decline(){
        runOnUiThread {
            Toast.makeText(this, "Доступ запрещён", Toast.LENGTH_SHORT).show()
        }
        finish()
    }

    @RequiresApi(Build.VERSION_CODES.N)
    override fun onResume() {
        super.onResume()
        checkAccess({
            updateChatState()
        }, {
            decline()
        })
    }

        //Добавляет сообщение в контейнер
    @RequiresApi(Build.VERSION_CODES.N)
    fun addMessage(info:MessageInfo) = runOnUiThread {
        var isOwn = info.fromID == Connector.instance.authorizedID
        val container = findViewById<LinearLayout>(R.id.chat_msgcont)
        val root = layoutInflater.inflate(R.layout.chats_message, container, false) as LinearLayout
        fun rem(id:Int){
            root.removeView(root.findViewById(id))
        }
        if(isOwn){
            rem(R.id.chats_rspace)
        }else{
            rem(R.id.chats_lspace)
        }
        val label = root.findViewById<TextView>(R.id.chats_pane_name)
        val text = root.findViewById<TextView>(R.id.chats_pane_content)
        var user = ID(info.fromID).getUserSync()
        if(user != null) {
            label.text = user.username
            text.text = info.content
            container.addView(root)
        }
    }

        //Запрос на сервер для загрузки чата
    fun updateInfo(gID:Long, onAccept:(GetGroupCallback.Out)->Unit){
        getGroupInfo(this, gID, onAccept)
    }
}