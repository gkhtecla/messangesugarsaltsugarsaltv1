package com.beenaxis.messangerv1.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.*
import com.beenaxis.lib.callbacks.sendCallback
import com.beenaxis.lib.callbacks.types.ErrorCallback
import com.beenaxis.lib.callbacks.types.SuccessCallback
import com.beenaxis.lib.callbacks.types.groups.CreateGroupCallback
import com.beenaxis.lib.utils.log
import com.beenaxis.messangerv1.R
import com.beenaxis.messangerv1.server.Connector
import java.util.ArrayList

/**
 * Создание группы
 */
class CreateGroupActivity : BaseActivity() {

    val members:MutableList<LinearLayout> = mutableListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_group)
        val add = findViewById<ImageButton>(R.id.creategroup_add)
        val create = findViewById<Button>(R.id.creategroup_accept)
        create.setOnClickListener {
            createGroup()
        }
        add.setOnClickListener {
            addPane()
        }

    }

    /**
     * Создание группы
     */
    fun createGroup(){
        val groupName = findViewById<EditText>(R.id.creategroup_nameedt)
        val logins = ArrayList<String>()
        members.forEach {
            var memberName = it.findViewById<EditText>(R.id.creategroup_pane_mname)
            logins.add(memberName.text.toString())
        }
        sendCallback(CreateGroupCallback(groupName.text.toString(), Connector.instance.authorizedID, logins, Connector.instance.authorizedKey!!)){
            if(it is SuccessCallback){
                finish()
            }else if(it is ErrorCallback){
                runOnUiThread {
                    Toast.makeText(this, "${it.error}", Toast.LENGTH_SHORT).show()
                }
            }
        }.send(Connector.instance.serverChannel!!)
    }


    /**
     * Добавление панели для участника
     */
    fun addPane(){
        var membersLayout = findViewById<LinearLayout>(R.id.creategroup_members)
        var root = layoutInflater.inflate(R.layout.creategroup_memberpane, membersLayout, false) as LinearLayout
        //var memberName = root.findViewById<EditText>(R.id.creategroup_pane_mname)
        var removeBtn = root.findViewById<ImageButton>(R.id.creategroup_removeown)
        removeBtn.setOnClickListener {
            members.remove(root)
            membersLayout.removeView(root)
        }
        membersLayout.addView(root)
        members.add(root)
    }
}