package com.beenaxis.messangerv1.activities.ui.groups

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import com.beenaxis.lib.callbacks.sendCallback
import com.beenaxis.lib.callbacks.types.ErrorCallback
import com.beenaxis.lib.callbacks.types.groups.GetGroupsCallback
import com.beenaxis.messangerv1.R
import com.beenaxis.messangerv1.activities.ChatActivity
import com.beenaxis.messangerv1.activities.CreateGroupActivity
import com.beenaxis.messangerv1.activities.ui.BaseFragment
import com.beenaxis.messangerv1.server.Connector
import com.beenaxis.messangerv1.utils.ID


class GroupsFragment : BaseFragment() {

    lateinit var root:LinearLayout
    lateinit var chats:LinearLayout

    fun toCreateGroup(){
        val intent = Intent(requireContext(), CreateGroupActivity::class.java)
        startActivity(intent)
    }

    fun toChat(groupID:Long){
        val intent = Intent(requireContext(), ChatActivity::class.java)
        intent.putExtra("groupID", groupID)
        startActivity(intent)
    }

    fun updateGroupList(){
        if(root == null)return
        chats = root.findViewById<LinearLayout>(R.id.chats)
        chats.removeAllViews()
        val createChat = root.findViewById<Button>(R.id.chats_createchat)
        createChat.setOnClickListener {
            toCreateGroup()
        }
        sendCallback(GetGroupsCallback(Connector.instance.authorizedKey!!)){
            if(it is GetGroupsCallback.Out){
                it.msgInfo.forEach {
                    var lastMsg = it.lastMsg
                    var name = it.name
                    if(lastMsg != null){
                        ID(lastMsg.fromID).getUser {lastUser->
                            addChat(name, "${lastUser?.username}:${lastMsg.content}"){
                                toChat(it.groupID)
                            }
                        }
                    }else{
                        addChat(name, "Нет сообщений"){
                            toChat(it.groupID)
                        }
                    }
                }
            }else if(it is ErrorCallback){
                requireActivity().runOnUiThread {
                    Toast.makeText(requireContext(), "Error: ${it.error}", Toast.LENGTH_SHORT).show()
                }
            }
        }.send(Connector.instance.serverChannel!!)
    }

    override fun onResume() {
        super.onResume()
        updateGroupList()
    }

    @RequiresApi(Build.VERSION_CODES.N)
    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        root = inflater.inflate(R.layout.fragment_groups, container, false) as LinearLayout
        //updateGroupList()
        return root
    }

    fun addChat(name:String, last:String, onClick:()->Unit){
        requireActivity().runOnUiThread{
            val newView = layoutInflater.inflate(R.layout.chat_pane, chats, false)
            val text = newView.findViewById<TextView>(R.id.chat_title)
            val lastname = newView.findViewById<TextView>(R.id.chat_last)
            text.text = name
            lastname.text = last
            newView.setOnClickListener {
                onClick.invoke()
            }
            text.text = name
            chats.addView(newView)
        }
    }
}