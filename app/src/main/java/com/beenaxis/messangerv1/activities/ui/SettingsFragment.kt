package com.beenaxis.messangerv1.activities.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import androidx.navigation.fragment.findNavController
import com.beenaxis.lib.callbacks.sendCallback
import com.beenaxis.lib.callbacks.types.ChangeNameCallback
import com.beenaxis.lib.callbacks.types.SuccessCallback
import com.beenaxis.messangerv1.R
import com.beenaxis.messangerv1.activities.AppActivity
import com.beenaxis.messangerv1.server.Connector

class SettingsFragment : BaseFragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_settings, container, false)
        val edit = view.findViewById<EditText>(R.id.settings_namechange_edit)
        val enter = view.findViewById<Button>(R.id.settings_changename_enter)
        enter.setOnClickListener {
            val newName = edit.text.toString()
            if(newName.isBlank()){
                requireActivity().runOnUiThread {
                    Connector.sendToast("Нельзя пустым")
                }
                return@setOnClickListener
            }
            if(newName.length > 16 || newName.length < 3){
                requireActivity().runOnUiThread {
                    Connector.sendToast("Имя должно быть размером от 3 до 16 символов")
                }
                return@setOnClickListener
            }
            sendCallback(ChangeNameCallback(newName, Connector.instance.authorizedKey!!)){
                if(it is SuccessCallback){
                    requireActivity().runOnUiThread {
                        Connector.sendToast("Имя успешно изменено!")
                        (requireActivity() as AppActivity).updateHeader()
                    }
                }else{
                    requireActivity().runOnUiThread {
                        Connector.sendToast("Произошла неизвестная ошибка")
                    }
                }
            }.send(Connector.instance.serverChannel!!)
        }
        return view
    }

}