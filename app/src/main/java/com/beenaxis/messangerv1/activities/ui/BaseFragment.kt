package com.beenaxis.messangerv1.activities.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment

open class BaseFragment : Fragment(){

    companion object{
        var currentFragment:BaseFragment? = null
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        currentFragment = this
        println("FRAGMENT CREATED")
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        println("FRAGMENT ON CREATE VIEW ${javaClass.simpleName}")
        return super.onCreateView(inflater, container, savedInstanceState)
    }

    override fun onStart() {
        println("FRAGMENT STARTED ${javaClass.simpleName}")
        super.onStart()
    }

    override fun onStop() {
        println("FRAGMENT STOPPED ${javaClass.simpleName}")
        super.onStop()
    }

    override fun onResume() {
        println("FRAGMENT RESUMED ${javaClass.simpleName}")
        super.onResume()
    }

    override fun onDestroy() {
        println("FRAGMENT DESTROYED ${javaClass.simpleName}")
        super.onDestroy()
        currentFragment = null
    }
}