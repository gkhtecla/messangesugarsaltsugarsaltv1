package com.beenaxis.messangerv1.activities

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import androidx.annotation.RequiresApi
import com.beenaxis.lib.Servers
import com.beenaxis.lib.callbacks.sendCallback
import com.beenaxis.lib.callbacks.types.AuthCallback
import com.beenaxis.lib.callbacks.types.ErrorCallback
import com.beenaxis.lib.callbacks.types.RegisterCallback
import com.beenaxis.lib.packets.Packet
import com.beenaxis.messangerv1.R
import com.beenaxis.messangerv1.server.Connector
import com.beenaxis.messangerv1.server.Connector.Companion.sendToast
import com.beenaxis.messangerv1.server.Connector.Companion.startConnect

/**
 * Окно авторизации, регистрации
 */
class MainActivity : BaseActivity() {

    lateinit var login:EditText
    lateinit var password:EditText
    lateinit var joinBtn:Button
    lateinit var registerBtn:Button

    fun toChat(){
        if(!Connector.instance.authorized)return
        val intent = Intent(this, AppActivity::class.java)
        startActivity(intent)
        this.finish()
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        startConnect(this, Runnable {})
        if(Connector.instance.authorized){
            toChat()
            return
        }
        setContentView(R.layout.activity_main)
        login = findViewById<EditText>(R.id.login)
        password = findViewById<EditText>(R.id.password)
        joinBtn = findViewById<Button>(R.id.authBtn)
        registerBtn = findViewById<Button>(R.id.registerBtn)
        registerBtn.isEnabled = true
        registerBtn.setOnClickListener {
            //Регистрация
            setEnabledInputs(false)
            sendCallback(RegisterCallback(
                    login = login.text.toString(),
                    password = password.text.toString(),
                    username = login.text.toString())){
                runOnUiThread{
                    if(it is ErrorCallback){
                        sendToast(it.error)
                    }else if(it is RegisterCallback.Out){
                        sendToast("Успешная регистрация!")
                    }
                    setEnabledInputs(true)
                }
            }.send(Connector.instance.serverChannel!!)
        }
        joinBtn.isEnabled = true
        joinBtn.setOnClickListener {
            //Авторизация
            setEnabledInputs(false)
            sendCallback(AuthCallback(
                    login = login.text.toString(),
                    password = password.text.toString()
            )){
                runOnUiThread {
                    if(it is AuthCallback.Success){
                        Connector.instance.authorized = true
                        Connector.instance.authorizedID = it.id
                        Connector.instance.authorizedKey = it.authedKey
                        toChat()
                    }else if(it is ErrorCallback){
                        setEnabledInputs(true)
                        sendToast(it.error)
                    }
                }
            }.send(Connector.instance.serverChannel!!)
        }
    }

    fun setEnabledInputs(iss:Boolean){
        setEnabledButtons(iss)
        setEnabledBoxes(iss)
    }

    fun setEnabledBoxes(iss:Boolean){
        login.isEnabled = iss
        password.isEnabled = iss
    }

    fun setEnabledButtons(iss:Boolean){
        joinBtn.isEnabled = iss
        registerBtn.isEnabled = iss
    }
}