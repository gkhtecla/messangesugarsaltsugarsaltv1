package com.beenaxis.messangerv1.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.*
import com.beenaxis.lib.UserInfo
import com.beenaxis.lib.callbacks.sendCallback
import com.beenaxis.lib.callbacks.types.ErrorCallback
import com.beenaxis.lib.callbacks.types.SuccessCallback
import com.beenaxis.lib.callbacks.types.groups.AddMemberCallback
import com.beenaxis.lib.callbacks.types.groups.GetGroupCallback
import com.beenaxis.lib.callbacks.types.groups.KickMemberCallback
import com.beenaxis.messangerv1.R
import com.beenaxis.messangerv1.server.Connector
import com.beenaxis.messangerv1.utils.*

/**
 * Информация о группе
 */
class GroupInfoActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val groupID = intent.extras?.getLong("groupID") ?: -1L
        if(groupID == -1L){
            finish()
        }
        setContentView(R.layout.activity_group_info)
        val btn = findViewById<ImageButton>(R.id.ginfo_addmemberbtn)
        val edt = findViewById<EditText>(R.id.ginfo_addmemberedt)
        btn.setOnClickListener {
            sendCallback(AddMemberCallback(groupID, edt.text.toString(), AUTH_KEY)){
                if(it is SuccessCallback){
                    runOnUiThread {
                        update()
                    }
                }else if(it is ErrorCallback){
                    runOnUiThread {
                        Toast.makeText(this, "${it.error}", Toast.LENGTH_SHORT).show()
                    }
                }
            }.send(SERVERCHANNEL)
        }
    }

    override fun onResume() {
        super.onResume()
        update()
    }

    fun update(){
        val gID = intent.extras?.getLong("groupID") ?: -1L
        if(gID == -1L)return
        findViewById<LinearLayout>(R.id.ginfo_memberscont).removeAllViews()
        updateInfo(gID){
            findViewById<TextView>(R.id.ginfo_title).text = it.name
            it.members.forEach { minfo->
                ID(minfo.id).getUser { uinfo->
                    if(uinfo == null)return@getUser
                    addMember(uinfo, minfo.role, gID)
                }
            }
        }
    }

    fun addMember(uinfo:UserInfo, role:String, gID:Long) = runOnUiThread {
        val container = findViewById<LinearLayout>(R.id.ginfo_memberscont)
        val root = layoutInflater.inflate(R.layout.ginfo_member, container, false)
        root.findViewById<ImageButton>(R.id.ginfo_member_kick).setOnClickListener {
            sendCallback(KickMemberCallback(gID, uinfo.id!!, AUTH_KEY)){
                if(it is SuccessCallback){
                    if(uinfo.id!! == Connector.instance.authorizedID){
                        finish()
                    }
                    runOnUiThread {
                        update()
                    }
                }else if(it is ErrorCallback){
                    runOnUiThread {
                        Toast.makeText(this, "${it.error}", Toast.LENGTH_SHORT).show()
                    }
                }
            }.send(SERVERCHANNEL)
        }
        root.findViewById<TextView>(R.id.ginfo_member_title).text = "${uinfo.username} | $role"
        container.addView(root)
    }

    fun updateInfo(gID:Long, onAccept:(GetGroupCallback.Out)->Unit){
        getGroupInfo(this, gID, onAccept)
    }
}