package com.beenaxis.messangerv1.utils

import android.widget.Toast
import com.beenaxis.lib.UserInfo
import com.beenaxis.lib.callbacks.sendCallback
import com.beenaxis.lib.callbacks.types.ErrorCallback
import com.beenaxis.lib.callbacks.types.GetUserInfo
import com.beenaxis.lib.callbacks.types.groups.GetGroupCallback
import com.beenaxis.messangerv1.activities.BaseActivity
import com.beenaxis.messangerv1.server.Connector
import io.netty.channel.Channel

val SERVERCHANNEL:Channel
    get() = Connector.instance.serverChannel!!

val AUTH_KEY:String
    get() = Connector.instance.authorizedKey!!

fun getUserInfo(id:ID, onAccept:(UserInfo?)->Unit){
    if(id.byID()){
        getUserInfo(id.id, onAccept)
    }
    if(id.byLogin()){
        getUserInfo(id.login!!, onAccept)
    }
}

fun getUserInfo(id:Long, onAccept:(UserInfo?)->Unit){
    sendCallback(GetUserInfo(id)){
        if(it is GetUserInfo.UserNotFound){
            println("User ${id} not found")
            onAccept.invoke(null)
        }else if(it is GetUserInfo.Out){
            println("user found")
            onAccept.invoke(it.userInfo)
        }else{
            onAccept.invoke(null)
        }
    }.send(Connector.instance.serverChannel!!)
}

fun getGroupInfo(activity:BaseActivity, gID:Long, onAccept:(GetGroupCallback.Out)->Unit){
    sendCallback(GetGroupCallback(gID, Connector.instance.authorizedKey!!)){
        if(it is ErrorCallback){
            activity.runOnUiThread {
                Toast.makeText(activity, "${it.error} ${gID}", Toast.LENGTH_SHORT).show()
            }
        }else if(it is GetGroupCallback.Out){
            onAccept.invoke(it)
        }
    }.send(Connector.instance.serverChannel!!)
}

fun getUserInfo(login:String, onAccept:(UserInfo?)->Unit){
    sendCallback(GetUserInfo(login)){
        if(it is GetUserInfo.UserNotFound){
            println("User ${login} not found")
            onAccept.invoke(null)
        }else if(it is GetUserInfo.Out){
            println("user found")
            onAccept.invoke(it.userInfo)
        }else{
            onAccept.invoke(null)
        }
    }.send(Connector.instance.serverChannel!!)
}