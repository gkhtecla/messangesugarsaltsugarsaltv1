package com.beenaxis.messangerv1.utils

import android.os.Build
import androidx.annotation.RequiresApi
import com.beenaxis.lib.UserInfo
import com.beenaxis.lib.packets.NettyUnit
import java.util.concurrent.CompletableFuture
import javax.persistence.Embeddable

/**
 * Айди любого юзера.
 * Проще пинговать
 */
@Embeddable
class ID : NettyUnit{

    var id:Long = -1
        private set
    var login:String? = null
        private set

    constructor(id:Long){
        this.id = id
    }

    constructor(login:String){
        this.login = login
    }

    /**
     * Проверки для получения юзеров
     * @see getUser()
     * @see getUserSync()
     */
    fun byID() : Boolean = id != -1L && login == null
    fun byLogin() : Boolean = id == -1L && login != null

    fun getUser(onAccept:(UserInfo?)->Unit){
        return getUserInfo(this, onAccept)
    }

    @RequiresApi(Build.VERSION_CODES.N)
    fun getUserSync() : UserInfo? {
        val future = CompletableFuture<UserInfo?>()
        getUser {
            future.complete(it)
        }
        return future.get()
    }
}