package com.beenaxis.messangerv1.server

import android.os.Build
import android.widget.Toast
import androidx.annotation.RequiresApi
import com.beenaxis.lib.Servers
import com.beenaxis.lib.callbacks.*
import com.beenaxis.lib.callbacks.types.NoCallback
import com.beenaxis.lib.packets.NettyUnit
import com.beenaxis.lib.packets.Packet
import com.beenaxis.lib.utils.log
import com.beenaxis.messangerv1.activities.BaseActivity
import io.netty.bootstrap.Bootstrap
import io.netty.channel.*
import io.netty.channel.nio.NioEventLoopGroup
import io.netty.channel.socket.SocketChannel
import io.netty.channel.socket.nio.NioSocketChannel
import io.netty.handler.codec.serialization.ClassResolvers
import io.netty.handler.codec.serialization.ObjectDecoder
import io.netty.handler.codec.serialization.ObjectEncoder
import java.util.concurrent.CompletableFuture
import java.util.concurrent.TimeUnit

/**
 * Подключает app >> server
 */
open class Connector : CallbackProcessor{
    var serverChannel: Channel? = null
    var authorized:Boolean = false //Авторизован?
    var authorizedID:Long = -1L //Текущий айди авторизованного пользователя
    var authorizedKey:String? = null //Ключ сессии
    var currentActivity:BaseActivity? = null//Нужен для того чтобы в BaseActivity отправлять обратные вызовы, для удобства
    private var group: NioEventLoopGroup? = null

    //Отправление пакета на сервер
    fun sendPacket(packet: Packet) {
        serverChannel?.writeAndFlush(packet) ?: log("unable to send packets")
    }

    //При подключении
    open fun onConnected() {}

    /*
        Пакет с сервера
     */
    fun packetFromServer(packet: Packet) {

    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    fun connect(): Boolean {
        group = NioEventLoopGroup()
        try {
            Bootstrap().group(group)
                    .channel(NioSocketChannel::class.java)
                    .option(ChannelOption.SO_KEEPALIVE, true)
                    .handler(object : ChannelInitializer<SocketChannel>() {
                        public override fun initChannel(socketChannel: SocketChannel) {
                            val pipeline = socketChannel.pipeline()
                            //Декодеры Байты->Обьект
                            pipeline.addLast(ObjectDecoder(ClassResolvers.weakCachingConcurrentResolver(NettyUnit::class.java.getClassLoader())))
                            pipeline.addLast(ObjectDecoder(ClassResolvers.cacheDisabled(null)))
                            //Энекодер Обьект->Байты
                            pipeline.addLast(ObjectEncoder())
                            pipeline.addLast(object : ChannelInboundHandlerAdapter() {
                                @Throws(Exception::class)
                                override fun channelActive(ctx: ChannelHandlerContext) {
                                    serverChannel = ctx.channel()
                                    onConnected()
                                    log("Подключён к серверу")
                                }

                                @Throws(Exception::class)
                                override fun channelRead(ctx: ChannelHandlerContext, msg: Any) {
                                    if (msg is Packet) {
                                        if(msg is CallbackPacket){
                                            val ret = onCallback(msg.callback)
                                            sendPacket(CallbackOutPacket(msg.idCallback, ret))
                                            return
                                        }
                                        if(msg is CallbackOutPacket){
                                            val emmiter = Callbacks.findEmmiter(msg.id)
                                            emmiter?.onRequest(msg.ret)
                                            return
                                        }
                                        packetFromServer(msg)
                                    }
                                }

                                @Throws(Exception::class)
                                override fun channelInactive(ctx: ChannelHandlerContext) {
                                    log("channelInactive")
                                    authorized = false
                                }

                                @Throws(Exception::class)
                                override fun exceptionCaught(ctx: ChannelHandlerContext, cause: Throwable) {
                                    cause.printStackTrace()
                                }

                                @Throws(Exception::class)
                                override fun channelUnregistered(ctx: ChannelHandlerContext) {
                                    log("channelUnregistered")
                                }
                            })
                        }
                    })
                    .connect(Servers.HOST, Servers.PORT) //Само подключения
                    .sync()
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return false
    }

    companion object {
        var instanced = false
        lateinit var instance: Connector
        var showen: BaseActivity? = null

        @JvmStatic
        @RequiresApi(api = Build.VERSION_CODES.N)
        fun startConnect(main: BaseActivity?, onConnected: Runnable): Connector? {
            if(instanced){
                return instance
            }
            showen = main
            val future = CompletableFuture<Void?>()
            instance = object : Connector() {
                override fun onConnected() {
                    onConnected.run()
                    future.complete(null)
                }
            }
            instance.connect()
            try {
                future.get(2, TimeUnit.SECONDS)
            } catch (e: Exception) {
                e.printStackTrace()
            }
            instanced = true
            return instance
        }

        fun sendToast(text: String?) {
            Toast.makeText(showen, text, Toast.LENGTH_SHORT).show()
        }
    }

    override fun onCallback(incoming: Callback): Callback {
        println("onCallback MAIN : $incoming")
        return currentActivity?.onCallback(incoming) ?: NoCallback()
    }
}