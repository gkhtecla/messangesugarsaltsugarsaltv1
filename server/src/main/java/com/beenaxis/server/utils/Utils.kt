package com.beenaxis.server.utils

import com.beenaxis.server.DedicatedServer
import com.beenaxis.server.database.groups.GroupsDB
import com.beenaxis.server.database.users.UsersDB

val USERDB: UsersDB
    get() {
        return DedicatedServer.SERVER.usersDB
    }

val GROUPSDB: GroupsDB
    get() {
        return DedicatedServer.SERVER.groupsDB
    }