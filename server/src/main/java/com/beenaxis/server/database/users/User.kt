package com.beenaxis.server.database.users

import com.beenaxis.lib.utils.PasswordHash
import com.beenaxis.server.database.IDirty
import javax.persistence.*

/*
    Юзер для хранения в БД
 */

@Entity
class User : IDirty{

    /*
        Добавлять в makeDirty() !!!
     */
    @Id @GeneratedValue
    var id:Long = 0
    lateinit var login:String
    lateinit var username:String
    lateinit var password:String
    @Transient
    var isNew:Boolean = false //Была загружена из базы или создана
    @OneToMany(fetch = FetchType.EAGER)
    lateinit var groups:ArrayList<Long> //Айдишники чатов

    constructor(login:String, username:String, password:String){
        this.login = login
        this.username = username
        this.password = PasswordHash.hashSHA(password) //Даже я не буду знать пароль, защита от утечки БД
        groups = ArrayList()
        isNew = true
    }

    constructor(){
        isNew = false
    }

    fun comparePassword(password:String) : Boolean{
        return this.password == PasswordHash.hashSHA(password)
    }

    override fun makeDirty() {
        dirty(login, "login")
        dirty(username, "username")
        dirty(groups, "groups")
        dirty(password, "password")
    }
}