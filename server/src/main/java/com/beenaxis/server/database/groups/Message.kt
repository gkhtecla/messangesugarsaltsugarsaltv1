package com.beenaxis.server.database.groups

import com.beenaxis.lib.callbacks.types.groups.MessageInfo
import javax.persistence.Embeddable

@Embeddable
class Message(from:Long, content:String, date:Long) {

    var fromID:Long = from
    var content:String = content
    var date:Long = date //In milliseconds

    fun buildMessageInfo() : MessageInfo {
        return MessageInfo(fromID, content, date)
    }
}