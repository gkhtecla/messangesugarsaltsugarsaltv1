package com.beenaxis.server.database.users

import com.beenaxis.lib.UserInfo
import com.beenaxis.lib.callbacks.AuthedCallback
import com.beenaxis.lib.callbacks.Callback
import com.beenaxis.lib.callbacks.sendCallback
import com.beenaxis.lib.callbacks.types.ChangeNameCallback
import com.beenaxis.lib.callbacks.types.ErrorCallback
import com.beenaxis.lib.callbacks.types.NoCallback
import com.beenaxis.lib.callbacks.types.SuccessCallback
import com.beenaxis.lib.callbacks.types.groups.*
import com.beenaxis.lib.packets.Packet
import com.beenaxis.lib.utils.PasswordHash
import com.beenaxis.lib.utils.addNoContain
import com.beenaxis.lib.utils.log
import com.beenaxis.server.Client
import com.beenaxis.server.DedicatedServer
import com.beenaxis.server.database.DataBase
import com.beenaxis.server.database.groups.*
import com.beenaxis.server.utils.GROUPSDB
import com.beenaxis.server.utils.USERDB
import java.util.ArrayList
import kotlin.random.Random

/*
    Юзер в кэше.
 */
class CachedUser(val cached:User) {
    /**
     * Текущий авторизованный клиент
     */
    var session:Client? = null
        set(value){
            field = value
            value?.attachedUser = this
        }

    /**
     * Это по сути метод который возвращает (авторизован ли юзер?)
     */
    val authorized:Boolean
        get() = session != null

    /**
     * Это указатель на бд юзеров
     */
    val userDB:UsersDB
        get() = DedicatedServer.SERVER.usersDB

    /**
     * Время которое он лежит в кэше
     */
    var cacheingTime:Long = 0L //Время в кэше
        set(value){
            if(value >= DedicatedServer.MAX_CACHEING_TIME && !authorized){
                log("Flushed user ${cached.login}")
                DedicatedServer.SERVER.usersDB.flushAndRemoveUser(this)
                return
            }
            field = value
        }

    /**
     * Ключ авторизации, генерируется на стороне сервера, и отправляется при успешной авторизации
     * юзеру.
     */
    var authedKey:String = PasswordHash.hashSHA(Random.nextInt(100000).toString())

    /**
     * Создаем UserInfo, который существует в обоих модулях
     * server и app, для того чтобы передавать его через netty
     */
    fun buildUserInfo() : UserInfo {
        return UserInfo().apply {
            login = cached.login
            id = cached.id
            username = cached.username
            isOnline = authorized
        }
    }

    /**
     * Отправление юзеру пакет
     */
    fun writeToClient(packet:Packet){
        session?.sendPacket(packet)
    }

    /**
     * Сохранение юзера в базу
     */
    fun save(){
        userDB.flushUser(this)
    }

    /**
     * Callback
     * Здесь мы точно знаем что пользователь отправил авторизованный пакет
     * Каждый из AuthedCallback должен иметь ключ
     * Чтобы другой пользователь не взломал исходники и не сделал что то
     * от лица другого юзера.
     */
    fun onAuthCallback(incom:AuthedCallback) : Callback{
        if(incom is KickMemberCallback){
            val group = GROUPSDB.loadOrGetGroup(incom.groupID)
            if(group != null){
                if(!group.cached.isOwner(cached.id)){
                    return ErrorCallback("Это доступно только ${Role.OWNER.prefix}")
                }
                val whoRem = USERDB.loadUserToCache(incom.whoRemove)
                if(whoRem == null){
                    return ErrorCallback("Данного юзера не существует ${incom.whoRemove}")
                }
                if(group.cached.isOwner(whoRem.cached.id)){
                    return ErrorCallback("Нельзя выгнать владельца")
                }
                if(group.cached.getMember(whoRem.cached.id) == null){
                    return ErrorCallback("Данного юзера нет в группе")
                }
                group.cached.members!!.remove(group.cached.getMember(whoRem.cached.id)!!)
                return SuccessCallback()
            }else{
                return ErrorCallback("Группа не найдена")
            }
        }
        if(incom is AddMemberCallback){
            val group = GROUPSDB.loadOrGetGroup(incom.groupID)
            if(group != null){
                if(!group.cached.isOwner(cached.id)){
                    return ErrorCallback("Это доступно только ${Role.OWNER.prefix}")
                }
                val whoAdd = USERDB.loadUserToCache(incom.whoAdd)
                if(whoAdd == null){
                    return ErrorCallback("Данного юзера не существует ${incom.whoAdd}")
                }
                if(group.cached.getMember(whoAdd.cached.id) != null){
                    return ErrorCallback("Данный юзер уже в группе.")
                }
                group.cached.members!!.add(Member(whoAdd.cached.id, Role.MEMBER))
                return SuccessCallback()
            }else{
                return ErrorCallback("Группа не найдена")
            }
        }
        if(incom is SendMessageCallback){
            val group = GROUPSDB.loadOrGetGroup(incom.groupID)
            if(group != null){
                if(group.cached.getMember(cached.id) == null){
                    return ErrorCallback("Вы не можете отправлять сообщения, так как вас нет " +
                            "в группе")
                }
                group.cached.members!!.forEach {
                    var onlineUser = DedicatedServer.SERVER.usersDB.getCached(it.id) ?: return@forEach
                    sendCallback(incom){
                        if(it is SuccessCallback){
                            println("Успешно отображён")
                        }
                    }.send(onlineUser.session?.channel ?: return@forEach)
                }
                group.cached.messages!!.add(Message(cached.id, incom.content, incom.date))
                if(group.cached.messages!!.size > 100){
                    group.cached.messages!!.remove(group.cached.messages!!.first())
                }
                println("message added ${incom.content}")
                return SuccessCallback()
            }else{
                return ErrorCallback("No group")
            }
        }
        if(incom is GetGroupCallback){
            val group = GROUPSDB.loadOrGetGroup(incom.id)
            if(group == null){
                return ErrorCallback("Группа не найдена")
            }else {
                val msgs = ArrayList<MessageInfo>()
                val members = ArrayList<MemberInfo>()
                val name = group.cached.name
                group.cached.members!!.forEach {
                    members.add(MemberInfo(it.id, it.role?.prefix ?: "NULL ROLE"))
                }
                group.cached.messages!!.forEach {
                    msgs.add(MessageInfo(it.fromID, it.content, it.date))
                }
                return GetGroupCallback.Out(name, msgs, members)
            }
        }
        if(incom is GetGroupsCallback){
            val list = ArrayList<GroupLabelInfo>()
            cached.groups.forEach {
                val group = GROUPSDB.loadOrGetGroup(it)
                if(group != null){
                    list.add(GroupLabelInfo(group.cached.name, group.getLastMessage()?.buildMessageInfo(), group.cached.id))
                }
            }
            return GetGroupsCallback.Out(list)
        }
        if(incom is CreateGroupCallback){
            var ownerUser = USERDB.loadUserToCache(incom.ownerID)
            if(ownerUser == null){
                return ErrorCallback("Создатель группы не найден")
            }
            var newGroup = Group(Member(ownerUser.cached.id, Role.OWNER), incom.name)
            val membersPinged:MutableList<CachedUser> = mutableListOf()
            var overlaps: HashMap<String, Int> = HashMap()
            incom.membersID.forEach {
                println("membercheck: $it")
                if(overlaps.containsKey(it)){
                    overlaps.put(it, overlaps[it]!!+1)
                }else{
                    overlaps.put(it, 1)
                }
            }
            var isOverlaps:String? = null
            overlaps.forEach { id, count ->
                println("postcheck: ${id} -> ${count}")
                if(count > 1 && isOverlaps == null){
                    isOverlaps = id
                }
            }
            if(isOverlaps != null){
                return ErrorCallback("Нельзя пригласить участника более 1 раза. (${isOverlaps})")
            }
            if(incom.membersID.size == 0){
                return ErrorCallback("Нельзя создать группу менее 2 участников.")
            }
            incom.membersID.forEach {
                val toMember = USERDB.loadUserToCache(it)
                if(toMember == null){
                    return ErrorCallback("Юзер @${it} не найден.")
                }else{
                    if(toMember.equals(this)){
                        return ErrorCallback("Нельзя пригласить самого себя")
                    }
                    newGroup.members!!.addNoContain(Member(toMember.cached.id, Role.MEMBER))
                    membersPinged.addNoContain(toMember)
                }
            }
            var cachedGroup = CachedGroup(newGroup)
            GROUPSDB.updateGroup(cachedGroup)
            membersPinged.forEach {
                it.cached.groups.addNoContain(newGroup.id)
            }
            ownerUser.cached.groups.addNoContain(newGroup.id)
            GROUPSDB.cachedGroups.add(cachedGroup)
            return SuccessCallback()
        }
        if(incom is ChangeNameCallback){
            cached.username = incom.newName
            return SuccessCallback()
        }
        return NoCallback()
    }

    fun flush(db:DataBase){
        if(cached.isNew){
            db.manager.persist(cached) //Добавляем обьект в БД
        }else{
            cached.makeDirty()//Обновляем
        }
    }
}