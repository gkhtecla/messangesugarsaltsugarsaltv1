package com.beenaxis.server.database.groups

import com.beenaxis.lib.utils.searchUnique
import com.beenaxis.server.database.DataBase

class GroupsDB : DataBase(GROUPS) {

    val cachedGroups:MutableList<CachedGroup> = mutableListOf()

    init{
        setup()
        refreshEntity(Group::class.java)
        refreshEmbed(Member::class.java)
    }

    fun getCachedGroup(id:Long) : CachedGroup? = cachedGroups.searchUnique { it.cached.id == id }

    fun loadOrGetGroup(id:Long) : CachedGroup? {
        val fromCache = getCachedGroup(id)
        if(fromCache != null) return fromCache
        val fromDB = loadGroupRaw(id) ?: return null
        val newCached = CachedGroup(fromDB)
        cachedGroups.add(newCached)
        return newCached
    }

    fun loadGroupRaw(id:Long) : Group?{
        val query = manager.createQuery("SELECT FROM Group g WHERE g.id = ${id}", Group::class.java)
        query.maxResults = 1
        val groups = query.resultList
        if(groups.size == 1){
            return groups.first()
        }else{
            return null
        }
    }

    fun updateGroup(group:CachedGroup){
        manager.transaction.begin()
        group.flush(this)
        manager.transaction.commit()
    }

    override fun close() {
        flushAll()
        super.close()
    }

    fun flushAll(){
        manager.transaction.begin()
        cachedGroups.forEach {
            it.flush(this)
        }
        manager.transaction.commit()
        cachedGroups.clear()
    }
}