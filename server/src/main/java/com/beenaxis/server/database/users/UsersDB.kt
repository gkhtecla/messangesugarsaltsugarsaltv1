package com.beenaxis.server.database.users

import com.beenaxis.lib.utils.searchUnique
import com.beenaxis.server.database.DataBase
import io.netty.channel.Channel
import kotlin.concurrent.thread

class UsersDB : DataBase {

    val users:MutableList<CachedUser> = mutableListOf()
    val cacheingThread:Thread

    constructor() : super(USERS){
        setup()
        refreshEntity(User::class.java)
        cacheingThread = thread {
            while(true){
                Thread.sleep(1000L)
                users.toMutableList().forEach {
                    it.cacheingTime++
                }
            }
        }
    }

    fun getCached(channel:Channel) : CachedUser? = users.searchUnique { it.session!!.channel == channel }
    fun getCached(login:String) : CachedUser? = users.searchUnique { it.cached.login == login }
    fun getCached(id:Long) : CachedUser? = users.searchUnique { it.cached.id == id }

    fun loadUserToCache(login:String) : CachedUser?{
        val current = getCached(login)
        if(current != null)return current
        val userdb = loadUserRaw(login) ?: return null
        val cu = CachedUser(cached = userdb)
        users.add(cu)
        return cu
    }

    fun loadUserToCache(id:Long) : CachedUser?{
        val current = getCached(id)
        if(current != null)return current
        val userdb = loadUserRaw(id) ?: return null
        val cu = CachedUser(cached = userdb)
        users.add(cu)
        return cu
    }

    fun loadUserRaw(id:Long) : User?{
        val query = manager.createQuery("SELECT FROM User u WHERE u.id = $id", User::class.java)
        query.maxResults = 1
        val results = query.resultList //Здесь происходит загрузка из БД
        if(results.size == 1){
            return results.first()
        }else{
            return null
        }
    }

    fun loadUserRaw(login:String) : User?{
        val query = manager.createQuery("SELECT FROM User u WHERE u.login = \"${login}\"", User::class.java)
        query.maxResults = 1
        val results = query.resultList //Здесь происходит загрузка из БД
        if(results.size == 1){
            return results.first()
        }else{
            return null
        }
    }

    fun flushAll(){
        manager.transaction.begin()
        users.forEach {
            it.flush(this)
        }
        manager.transaction.commit()
        users.clear()
    }

    fun flushAndRemoveUser(user:CachedUser){
        flushUser(user)
        users.remove(user)
    }

    override fun close() {
        flushAll()
        super.close()
    }

    val mutex:Any = Any()

    fun flushUser(user:CachedUser){
        synchronized(mutex){
            manager.transaction.begin()
            user.flush(this)
            manager.transaction.commit()
        }
    }
}