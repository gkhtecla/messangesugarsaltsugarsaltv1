package com.beenaxis.server.database

import javax.persistence.EntityManager
import javax.persistence.EntityManagerFactory
import javax.persistence.Persistence


/**
 * Как работает ObjectDB:
 * В нём есть контекст персистентности
 * где лежат обьекты которые соеденены с базой
 * именно поэтому можно без каких либо проблем любой обьект менять
 * например в кэше юзеров, меняешь название, коммитишь, и всё сохраняется.
 * Обязательно нужно помечать поля как Dirty
 * @see IDirty
 */
open class DataBase(var filePath:String) {

    companion object{
        val PATH_OFFSET = "C:/Users/beena/Desktop/MsgerTemp/"
        val USERS:String = "users.odb"
        val GROUPS:String = "groups.odb"
    }

    var isClosed:Boolean = false
    lateinit var factory: EntityManagerFactory
    lateinit var manager: EntityManager
    var created = false

    init{
        //Фикс проблемы с класс лоадером
        Thread.currentThread().setContextClassLoader(this.javaClass.classLoader)
        filePath = PATH_OFFSET+filePath
    }

    open fun close() {
        try {
            manager.clear()
            manager.close()
            factory.close()
            isClosed = true
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun setup(){
        factory = Persistence.createEntityManagerFactory(filePath)
        manager = factory.createEntityManager()
        created = true
    }

    fun <T> refreshEmbed(refresh: Class<T>?) {
        manager.metamodel.embeddable(refresh)
    }

    fun <T> refreshEntity(refresh: Class<T>?) {
        manager.metamodel.entity(refresh)
    }

}