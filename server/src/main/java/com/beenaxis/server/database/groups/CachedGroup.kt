package com.beenaxis.server.database.groups

import com.beenaxis.server.database.DataBase

class CachedGroup(val cached:Group) {

    fun flush(db:DataBase){
        if(cached.isNew){
            db.manager.persist(cached)
        }else{
            cached.makeDirty()
        }
    }

    fun getLastMessage() : Message?{
        if(cached.messages!!.size > 0){
            return cached.messages!!.last()
        }else{
            return null
        }
    }
}