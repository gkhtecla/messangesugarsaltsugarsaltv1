package com.beenaxis.server.database.groups

import javax.persistence.Embeddable
import javax.persistence.EnumType
import javax.persistence.Enumerated

@Embeddable
class Member {


    var id:Long = 0
    @Enumerated(EnumType.ORDINAL)
    var role:Role? = null

    constructor(id:Long, role:Role){
        this.id = id
        this.role = role
    }

    constructor(){

    }
}