package com.beenaxis.server.database

import javax.jdo.JDOHelper

/**
 * В JDO нужно помечать "грязные обьекты" чтобы база их обновила
 */
interface IDirty {
    fun makeDirty()
    fun dirty(obj:Any, field:String){//Делает поля 'грязными' чтобы они при commit обновлялись в БД
        JDOHelper.makeDirty(obj, field)
    }
}