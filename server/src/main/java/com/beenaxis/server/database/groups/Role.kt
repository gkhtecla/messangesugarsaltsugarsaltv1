package com.beenaxis.server.database.groups

import javax.persistence.Embeddable

@Embeddable
enum class Role(var prefix:String) {
    MEMBER("Участник"), ADMIN("Админ") ,OWNER("Владелец");
}