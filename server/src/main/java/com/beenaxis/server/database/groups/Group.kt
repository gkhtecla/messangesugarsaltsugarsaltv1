package com.beenaxis.server.database.groups

import com.beenaxis.lib.utils.searchUnique
import com.beenaxis.server.database.IDirty
import java.util.ArrayList
import javax.persistence.*


/**
 * Группа - любой чат с любым количеством юзеров
 * Чатов отдельно не будет
 */

@Entity
class Group : IDirty{

    @Id @GeneratedValue
    var id:Long = 0L
    var name:String = "Использовался пустой конструктор"
    @OneToMany(fetch = FetchType.EAGER)
    var messages:ArrayList<Message>? = null
    @OneToMany(fetch = FetchType.EAGER)
    var members:ArrayList<Member>? = null
    @Transient
    var isNew:Boolean

    constructor(owner:Member, name:String){
        members = ArrayList()
        messages = ArrayList()
        this.name = name
        owner.role = Role.OWNER
        members!!.add(owner)
        isNew = true
    }

    constructor(){
        isNew = false
    }

    fun getMember(id:Long) : Member? = members!!.searchUnique { it.id == id }
    fun isOwner(id:Long) : Boolean {
        return getMember(id)?.role == Role.OWNER
    }

    override fun makeDirty() {
        dirty(name, "name")
        dirty(messages!!, "messages")
        dirty(members!!, "members")
    }
}