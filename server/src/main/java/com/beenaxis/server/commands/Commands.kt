package com.beenaxis.server.commands

import com.beenaxis.lib.utils.log
import com.beenaxis.lib.utils.searchUnique
import com.beenaxis.server.DedicatedServer
import com.sun.xml.internal.fastinfoset.util.StringArray
import java.lang.Exception
import kotlin.concurrent.thread

/**
 * Консольные команды для сервера
 */
object Commands {


    val cmds:MutableList<Command> = mutableListOf()
    lateinit var executor:Thread

    init{
        cmds.add(object : Command("stop"){
            override fun execute(args: Array<String>) {
                log("Stopping server...")
                DedicatedServer.SERVER.stop()
                log("Server stopped")
                stop()
            }
        })
        cmds.add(object : Command("list"){
            override fun execute(args: Array<String>) {
                log("Users (${DedicatedServer.SERVER.usersDB.users.size}):")
                DedicatedServer.SERVER.usersDB.users.forEach {
                    log("Юзер: ${it.cached.login} - ${ if (it.authorized) "AUTHED" else "NO_AUTH"}")
                }
            }
        })
    }

    fun getCommand(label:String) : Command?{
        return cmds.searchUnique { it.label == label }
    }

    fun stop(){
        executor?.stop()
    }

    /**
     * Запускаем поток который ожидает строку
     */
    fun run(){
        executor = thread {
            try {
                while (true) {
                    //Происходит магия парсера
                    val next = readLine()
                    if (next == null || next.isBlank()) return@thread
                    val argsRaw = next.split("[\\s]+".toRegex())
                    val label = argsRaw[0]
                    val command = getCommand(label)
                    if (command != null) {
                        val args = mutableListOf<String>()
                        for (c in 1 until argsRaw.size) {
                            args.add(argsRaw[c])
                        }
                        //Вызываем команду на обработку
                        command.execute(args.toTypedArray())
                    } else {
                        println("Команда не найдена")
                    }
                }
            }catch (e:Exception){
                e.printStackTrace()
            }
        }
    }
}

abstract class Command(val label:String){

    abstract fun execute(args:Array<String>)
}