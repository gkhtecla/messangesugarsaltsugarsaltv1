package com.beenaxis.server

import com.beenaxis.lib.Servers
import com.beenaxis.lib.callbacks.CallbackOutPacket
import com.beenaxis.lib.callbacks.CallbackPacket
import com.beenaxis.lib.callbacks.Callbacks
import com.beenaxis.lib.packets.NettyUnit
import com.beenaxis.lib.packets.Packet
import com.beenaxis.lib.utils.log
import com.beenaxis.lib.utils.searchUnique
import com.beenaxis.server.commands.Commands
import com.beenaxis.server.database.groups.GroupsDB
import com.beenaxis.server.database.users.UsersDB
import io.netty.bootstrap.ServerBootstrap
import io.netty.channel.*
import io.netty.channel.nio.NioEventLoopGroup
import io.netty.channel.socket.SocketChannel
import io.netty.channel.socket.nio.NioServerSocketChannel
import io.netty.handler.codec.serialization.ClassResolvers
import io.netty.handler.codec.serialization.ObjectDecoder
import io.netty.handler.codec.serialization.ObjectEncoder
import kotlin.concurrent.thread
import kotlin.system.exitProcess


/**
 * Класс сервера
 */
class DedicatedServer {
    companion object {

        val MAX_CACHEING_TIME = 15L //Секундах
        lateinit var SERVER:DedicatedServer

        /**
         * При запуске server.jar выполняется это
         */
        @JvmStatic fun main(args : Array<String>) {
            if(Servers.isUnknownIP()){
                log("Поменяйте IP в Servers.kt")
                return
            }
            log("Запуск сервера... ${Servers.HOST}:${Servers.PORT}")
            Commands.run()
            startup()
        }

        fun startup(){
            SERVER = DedicatedServer()
            SERVER.runServer()
        }
    }

    val clients:MutableList<Client> = mutableListOf()
    val group: EventLoopGroup = NioEventLoopGroup()
    val usersDB:UsersDB
    val groupsDB:GroupsDB

    init{
        log("Подключение к ДБ...")
        usersDB = UsersDB()
        groupsDB = GroupsDB()
        log("Успешное подключение к базам данных.")
    }

    fun getClient(channel: Channel) : Client? = clients.searchUnique { it.channel == channel }

    var stopped:Boolean = false

    fun stop(){
        group.shutdownGracefully()
    }

    /**
     * Во время выключения
     */
    fun postStop(){
        if(stopped)return
        usersDB.close()
        groupsDB.close()
        stopped = true
    }

    fun runServer(){
        try {
            try {
                /**
                 * Создание Netty сервера, биндим порт и так далее.
                 */
                ServerBootstrap().group(group)
                        .channel(NioServerSocketChannel::class.java)
                        .childHandler(object : ChannelInitializer<SocketChannel>() {
                            override fun initChannel(socketChannel: SocketChannel) {
                                val pipeline: ChannelPipeline = socketChannel.pipeline()
                                pipeline.addLast(ObjectDecoder(ClassResolvers.weakCachingConcurrentResolver(NettyUnit::class.java.classLoader)))
                                pipeline.addLast(ObjectEncoder())
                                pipeline.addLast(ObjectDecoder(ClassResolvers.cacheDisabled(null)))
                                pipeline.addLast(object : ChannelInboundHandlerAdapter() {
                                    @Throws(Exception::class)
                                    override fun handlerAdded(ctx: ChannelHandlerContext) {
                                        val incoming: Channel = ctx.channel()
                                        val user = Client(incoming)
                                        println("User connected $incoming")
                                        clients.add(user)
                                    }

                                    @Throws(Exception::class)
                                    override fun handlerRemoved(ctx: ChannelHandlerContext) {
                                        if(usersDB.isClosed)return
                                        val incoming = ctx.channel()
                                        val h = getClient(incoming)
                                        if (h != null) {
                                            val currentUser = SERVER.usersDB.getCached(h.channel)
                                            if(currentUser != null){
                                                SERVER.usersDB.flushAndRemoveUser(currentUser)
                                            }
                                            clients.remove(h)
                                            println("User disconnect $incoming")
                                        }
                                    }

                                    override fun channelRead(ctx: ChannelHandlerContext, msg: Any) { //Клиент отправил сюда
                                        //TODO Прием обьектов от клиента
                                        val who: Channel = ctx.channel()
                                        val h = getClient(who)
                                        if (h != null) {
                                            if (msg is Packet) {
                                                //Похожий код содержиться в Клиентском классе Connector
                                                if(msg is CallbackPacket){
                                                    val ret = h.onCallback(msg.callback)//Обрабатываем вызов
                                                    h.sendPacket(CallbackOutPacket(msg.idCallback, ret))//Затем сразу отправляем обратно
                                                    return
                                                }
                                                if(msg is CallbackOutPacket){//Ответ от клиента
                                                    val emmiter = Callbacks.findEmmiter(msg.id)
                                                    emmiter?.onRequest(msg.ret)
                                                    return
                                                }
                                                h.onPacketIncoming(msg)
                                            }
                                        }
                                    }

                                    override fun exceptionCaught(ctx: ChannelHandlerContext, cause: Throwable) {
                                        cause.printStackTrace()
                                        ctx.channel().close()
                                    }
                                })
                            }
                        })
                        .bind(Servers.PORT)
                        .sync()
                        .channel()
                        .closeFuture()
                        .syncUninterruptibly()
            } catch (e: InterruptedException) {
                e.printStackTrace()
            }
        } finally {
            group.shutdownGracefully()
        }
        postStop()
        exitProcess(1)
    }
}