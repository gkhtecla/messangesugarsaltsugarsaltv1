package com.beenaxis.server

import com.beenaxis.lib.callbacks.AuthedCallback
import com.beenaxis.lib.callbacks.Callback
import com.beenaxis.lib.callbacks.CallbackProcessor
import com.beenaxis.lib.callbacks.NoAuthedCallback
import com.beenaxis.lib.callbacks.types.*
import com.beenaxis.lib.packets.Packet
import com.beenaxis.server.database.users.CachedUser
import com.beenaxis.server.database.users.User
import com.beenaxis.server.database.users.UsersDB
import io.netty.channel.Channel

/**
 * Клиент управляемый Netty
 */
class Client(val channel: Channel) : CallbackProcessor {
    companion object{
        val ERROR_LOGINORPASSWORD = "Не верный логин или пароль!"
    }

    val usersDB:UsersDB
        get() = DedicatedServer.SERVER.usersDB

    var attachedUser:CachedUser? = null

    fun sendPacket(packet:Packet){
        channel.writeAndFlush(packet)
    }

    fun onPacketIncoming(fromClient:Packet){

    }

    /**
     * Главная ветка Callback ов
     */
    override fun onCallback(incom: Callback): Callback {
        if(incom is AuthedCallback){
            if(attachedUser != null && attachedUser!!.authedKey == incom.key){
                return attachedUser!!.onAuthCallback(incom)
            }else{
                return NoAuthedCallback()
            }
        }
        if(incom is GetUserInfo){
            var userr:CachedUser? = null
            if(incom.id != -1L){
                userr = usersDB.loadUserToCache(incom.id)
            }else if(incom.login != null){
                userr = usersDB.loadUserToCache(incom.login!!)
            }
            if(userr != null){
                return GetUserInfo.Out(userr.buildUserInfo())
            }else{
                return GetUserInfo.UserNotFound()
            }
        }
        if(incom is RegisterCallback){
            var user = usersDB.loadUserToCache(incom.login)
            if(user == null){
                var newUser = User(incom.login, incom.username, incom.password)
                var cUser = CachedUser(cached = newUser)
                cUser.session = this
                usersDB.users.add(cUser)
                usersDB.flushUser(cUser)
                return RegisterCallback.Out()
            }else{
                return ErrorCallback("Юзер с таким логином уже существует!")
            }
        }
        if(incom is AuthCallback){
            var user = usersDB.loadUserToCache(incom.login)
            if(user != null){
                if(user.cached.comparePassword(incom.password)){
                    user.session = this
                    return AuthCallback.Success(user.authedKey, user.cached.id)
                }else{
                    return ErrorCallback(ERROR_LOGINORPASSWORD)
                }
            }else{
                return ErrorCallback(ERROR_LOGINORPASSWORD)
            }
        }
        return NoCallback()
    }
}