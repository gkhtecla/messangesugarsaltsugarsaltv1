package com.beenaxis.lib

/**
 * Тут информация о сервере
 */
object Servers {
    val INSERT = "<INSERT IP>"

    val PORT = 5500 //53000
    //Здесь нужно указать айпи сервера
    val HOST = "85.10.209.130" //64

    fun isUnknownIP() : Boolean {
        return HOST == INSERT
    }
}