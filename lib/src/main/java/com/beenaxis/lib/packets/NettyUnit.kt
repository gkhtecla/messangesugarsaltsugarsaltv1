package com.beenaxis.lib.packets

import java.io.Serializable

/**
 * Любой класс имплементирующий этот интерфейс можно
 * передовать через каналы (Netty)
 */
interface NettyUnit : Serializable