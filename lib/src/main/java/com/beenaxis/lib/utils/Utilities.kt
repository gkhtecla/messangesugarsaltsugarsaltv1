package com.beenaxis.lib.utils

import java.nio.charset.StandardCharsets
import java.security.MessageDigest


/*
    Хэширование паролей
 */
object PasswordHash{

    val SALT:String = "RDk3f7D83dD" //Нельзя менять! (соль нужна чтобы сложно было взломать)

    /**
     * SHA256 шифрование
     */
    fun hashSHA(input:String) : String{
        val digest: MessageDigest = MessageDigest.getInstance("SHA-256")
        val encodedhash: ByteArray = digest.digest(
                (input + SALT).toByteArray(StandardCharsets.UTF_8))
        return hashSHA256Bytes(encodedhash)
    }

    fun hashSHA256Bytes(hash:ByteArray) : String{
        val hexString = StringBuilder(2 * hash.size)
        for (element in hash) {
            val hex = Integer.toHexString(0xff and (element.toInt()))
            if (hex.length == 1) {
                hexString.append('0')
            }
            hexString.append(hex)
        }
        return hexString.toString()
    }
}


/**
 * Простое логирование
 */
fun log(s:String){
    println(s)
}

fun err(s:String){
    println(s)
}

