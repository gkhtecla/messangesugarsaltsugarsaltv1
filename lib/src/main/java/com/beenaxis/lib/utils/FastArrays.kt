package com.beenaxis.lib.utils

/**
 * Поиск обьекта по условию
 * Такой алгоритм есть везде. Чтобы много раз не писать одно и тоже
 * лучше вынести его отдельно.
 */
fun <T> List<T>.searchUnique(blok:(T)->Boolean) : T?{
    forEach{
        if(blok.invoke(it)){
            return it
        }
    }
    return null
}

/**
 * Чтобы не дублировать ссылку в списке на обьект
 */
fun <T> MutableList<T>.addNoContain(obj:T){
    if(contains(obj))return
    add(obj)
}
