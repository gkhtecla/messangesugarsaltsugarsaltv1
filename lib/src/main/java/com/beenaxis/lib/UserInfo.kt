package com.beenaxis.lib

import com.beenaxis.lib.packets.NettyUnit

/**
 * Информация о юзере
 */
class UserInfo : NettyUnit {

    var login:String? = null
    var username:String? = null
    var id:Long? = null
    var isOnline:Boolean = false
}