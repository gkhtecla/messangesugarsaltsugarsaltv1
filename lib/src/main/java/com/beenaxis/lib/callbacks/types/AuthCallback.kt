package com.beenaxis.lib.callbacks.types

import com.beenaxis.lib.callbacks.Callback

/**
 * Авторизация
 */
class AuthCallback(login:String, password:String) : Callback() {

    var login:String = login
    var password:String = password

    class Success(var authedKey:String, var id:Long) : Callback(){

    }
}