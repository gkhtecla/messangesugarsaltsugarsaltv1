package com.beenaxis.lib.callbacks.types.groups

import com.beenaxis.lib.callbacks.AuthedCallback

/**
 * Отправление сообщений
 * Работает так:
 * app->server-> apps.foreach { it->app }
 * Не возвращает ничего полезного
 */
class SendMessageCallback(var groupID:Long, var fromID:Long, var content:String, var date:Long, key:String) : AuthedCallback(key) {
}