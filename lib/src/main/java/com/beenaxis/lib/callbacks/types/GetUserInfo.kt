package com.beenaxis.lib.callbacks.types

import com.beenaxis.lib.UserInfo
import com.beenaxis.lib.callbacks.Callback

/**
 * Для получения UserInfo
 */
class GetUserInfo : Callback {

    var id:Long = -1
    var login:String? = null

    constructor(id:Long) : super(){
        this.id = id
    }

    constructor(login:String) : super(){
        this.login = login
    }

    class UserNotFound() : Callback()

    class Out(var userInfo:UserInfo) : Callback(){

    }
}