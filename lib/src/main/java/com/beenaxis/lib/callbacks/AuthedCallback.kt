package com.beenaxis.lib.callbacks

/**
 * Авторизованный вызов
 */
open class AuthedCallback( key:String) : Callback() {
    var key:String = key
}