package com.beenaxis.lib.callbacks

import com.beenaxis.lib.utils.searchUnique
import io.netty.channel.Channel

/**
 * Система обратных вызовов
 * app->server(onCallback)->app
 * server->app(onCallback)->server
 */
object Callbacks {

    private var id: Long = 0
    fun nextID(): Long {
        var i = id
        id++
        return i
    }

    val waitingCallbacks: MutableList<CallbackEmmiter> = mutableListOf()

    fun findEmmiter(id: Long): CallbackEmmiter? {
        return waitingCallbacks.searchUnique { it.id == id }
    }

}

interface CallbackProcessor{
    fun onCallback(incoming:Callback) : Callback
}

/**
 * Высокоуровневый метод (статичный)
 */
fun sendCallback(callback:Callback, onAccept: (Callback)->Unit) : CallbackEmmiter{
    val call = CallbackEmmiter(callback, onAccept)
    Callbacks.waitingCallbacks.add(call)
    return call
}

/**
 * Класс управления для sendCallback()
 */
class CallbackEmmiter(val callback:Callback, val onAccept: (Callback) -> Unit) {
    var id:Long = Callbacks.nextID()

    fun onRequest(out:Callback){
        onAccept.invoke(out)
        Callbacks.waitingCallbacks.remove(this)
    }

    /**
     * Отправляем вызов на канал
     */
    fun send(channel:Channel){
        channel.writeAndFlush(CallbackPacket(callback, id))//writeAndFlush - отправление обьекта
    }
}
