package com.beenaxis.lib.callbacks.types

import com.beenaxis.lib.callbacks.Callback

/**
 * Регистрация пользователя
 */
class RegisterCallback(var login:String, var username:String, var password:String) : Callback() {


    /**
     * Ответ
     */
    class Out : Callback(){

    }
}