package com.beenaxis.lib.callbacks.types

import com.beenaxis.lib.callbacks.Callback

/**
 * Пустой, показывает что ничего не произошло
 */
class NoCallback : Callback(){
}