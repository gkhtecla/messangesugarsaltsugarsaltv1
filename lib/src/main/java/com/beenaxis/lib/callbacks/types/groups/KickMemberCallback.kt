package com.beenaxis.lib.callbacks.types.groups

import com.beenaxis.lib.callbacks.AuthedCallback

class KickMemberCallback(var groupID:Long, var whoRemove:Long, key:String) : AuthedCallback(key) {
}