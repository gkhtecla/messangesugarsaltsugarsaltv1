package com.beenaxis.lib.callbacks.types.groups

import com.beenaxis.lib.callbacks.AuthedCallback
import com.beenaxis.lib.callbacks.Callback
import java.util.ArrayList

/**
 * Получить полную информацию о группе
 */
class GetGroupCallback(var id:Long, key:String) : AuthedCallback(key) {

    class Out(var name:String, var messages:ArrayList<MessageInfo>, var members:ArrayList<MemberInfo>) : Callback(){

    }
}