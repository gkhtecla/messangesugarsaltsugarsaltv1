package com.beenaxis.lib.callbacks

import com.beenaxis.lib.packets.Packet

/**
 * Обёртка для обратного вызова
 */
class CallbackPacket(callback:Callback, idCallback:Long) : Packet(){
    var idCallback:Long = idCallback
    var callback:Callback = callback
}