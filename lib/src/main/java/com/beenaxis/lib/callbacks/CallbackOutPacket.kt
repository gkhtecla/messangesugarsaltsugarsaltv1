package com.beenaxis.lib.callbacks

import com.beenaxis.lib.packets.Packet

/**
 * Обёртка для обратного вызова (назад)
 */
class CallbackOutPacket(var id:Long, var ret:Callback) : Packet() {
}