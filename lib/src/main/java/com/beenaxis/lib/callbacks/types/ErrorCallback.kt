package com.beenaxis.lib.callbacks.types

import com.beenaxis.lib.callbacks.Callback

/**
 * Ошибка при onCallback()
 */
class ErrorCallback(var error:String?) : Callback() {
    val hasError:Boolean
        get() = error != null
}