package com.beenaxis.lib.callbacks.types

import com.beenaxis.lib.callbacks.Callback

/**
 * Вызывается при успешных операциях в onCallback()
 */
class SuccessCallback : Callback() {
}