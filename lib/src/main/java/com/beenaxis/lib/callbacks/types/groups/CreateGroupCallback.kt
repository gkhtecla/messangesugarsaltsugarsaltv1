package com.beenaxis.lib.callbacks.types.groups

import com.beenaxis.lib.callbacks.AuthedCallback
import java.util.ArrayList

/**
 * Создать группу
 */
class CreateGroupCallback(var name:String, var ownerID:Long, var membersID:ArrayList<String>, key:String) : AuthedCallback(key) {


}