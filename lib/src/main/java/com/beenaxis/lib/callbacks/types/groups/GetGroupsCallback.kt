package com.beenaxis.lib.callbacks.types.groups

import com.beenaxis.lib.callbacks.AuthedCallback
import com.beenaxis.lib.callbacks.Callback
import java.util.ArrayList

/**
 * Получить краткую информацию о группе (для списка групп)
 */
class GetGroupsCallback(key:String) : AuthedCallback(key) {


    class Out(var msgInfo:ArrayList<GroupLabelInfo>) : Callback(){

    }
}