package com.beenaxis.lib.callbacks.types

import com.beenaxis.lib.callbacks.AuthedCallback
import com.beenaxis.lib.callbacks.Callback

/**
 * Изменить имя юзера
 */
class ChangeNameCallback(newName:String, key:String) : AuthedCallback(key) {
    var newName:String = newName
}