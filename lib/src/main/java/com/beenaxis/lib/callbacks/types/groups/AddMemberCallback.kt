package com.beenaxis.lib.callbacks.types.groups

import com.beenaxis.lib.callbacks.AuthedCallback

class AddMemberCallback(val groupID:Long, val whoAdd:String, key:String) : AuthedCallback(key) {
}