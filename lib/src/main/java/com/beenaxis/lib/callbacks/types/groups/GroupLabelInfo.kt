package com.beenaxis.lib.callbacks.types.groups

import com.beenaxis.lib.packets.NettyUnit

class GroupLabelInfo(var name:String, var lastMsg:MessageInfo?, var groupID:Long) : NettyUnit {
}